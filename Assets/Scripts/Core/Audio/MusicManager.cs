﻿using UnityEngine;
using System.Collections;
using N_Core.N_Singletons;

namespace N_Core.N_Audio
{
  /// <summary>
  ///   Class that handle background music in the game.
  /// </summary>
  public abstract class MusicManager<TInstance> : UnitySingleton<TInstance> where TInstance : Component
  {
    #region Protected and private fields

    /// <summary>
    ///   Audio source.
    /// </summary>
    private AudioSource _audioSource;

    #endregion

    #region Public methods

    /// <summary>
    ///   Play clip.
    /// </summary>
    /// <param name="clip">Clip to play</param>
    /// <param name="delay">Delay before playing clip</param>
    /// <param name="is_looped">Flag if clip is looped</param>
    public void ClipPlay(AudioClip clip, float delay, bool is_looped)
    {
      StartCoroutine(ClipPlayWithDelay(clip, delay, is_looped));
    }

    /// <summary>
    ///   Pause clip.
    /// </summary>
    public void ClipPause()
    {
      _audioSource.Stop();
    }

    /// <summary>
    ///   Change volume.
    /// </summary>
    /// <param name="volume">New music volume</param>
    public void VolumeChange(float volume)
    {
      _audioSource.volume = volume;
    }

    /// <summary>
    ///   Play clip at point.
    /// </summary>
    /// <param name="pos">Position where to play clip</param>
    /// <param name="spatial_blend">Spatial blend</param>
    /// <param name="clip">Clip to play</param>
    /// <returns>Audio source that plays clip</returns>
    public AudioSource ClipPlayAtPoint(Vector3 pos, float spatial_blend, AudioClip clip)
    {
      // Create game object.
      GameObject go = new GameObject("TmpAudio");
      // Set position.
      go.transform.position = pos;
      // Add audio source.
      AudioSource audioSource = go.AddComponent<AudioSource>();
      // Set spatial blend.
      audioSource.spatialBlend = spatial_blend;
      // Set clip.
      audioSource.clip = clip;
      // Play clip.
      audioSource.Play();
      // Destroy game object after clip is ended.
      Destroy(go, clip.length);
      // Return audio source (will give ability to change more properties of this audio source).
      return audioSource;
    }

    #endregion

    #region Protected and private methods

    /// <summary>
    ///   Start.
    /// </summary>
    private void Start()
    {
      // Get audio source.
      _audioSource = Instance.GetComponent<AudioSource>();
      // Set volume from player preferences.
      //_audioSource.volume = PlayerPrefsManager.MasterVolumeGet();
    }

    /// <summary>
    ///   Play clip with delay.
    /// </summary>
    /// <param name="clip">Clip to play</param>
    /// <param name="delay">Delay before playing clip</param>
    /// <param name="is_looped">Flag if clip is looped</param>
    /// <returns></returns>
    private IEnumerator ClipPlayWithDelay(AudioClip clip, float delay, bool is_looped)
    {
      // If no clip then exit form function.
      if(clip == null)
      {
        yield break;
      }
      // Wait for seconds.
      yield return new WaitForSeconds(delay);
      // Load clip.
      _audioSource.clip = clip;
      // Set loop on clip.
      _audioSource.loop = is_looped;
      // Play clip.
      _audioSource.Play();
    }

    #endregion
  }

  /// <summary>
  ///   Class that handle background music in the game.
  /// </summary>
  public class MusicManager : MusicManager<MusicManager>
  {
  }

}