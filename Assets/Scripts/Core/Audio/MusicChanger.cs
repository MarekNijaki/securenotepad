﻿using UnityEngine;

namespace N_Core.N_Audio
{
  /// <summary>
  ///   Class that handle changing background music in the game.
  /// </summary>
  public class MusicChanger : MonoBehaviour
  {
    #region Serialized fields

    /// <summary>
    ///   Clip.
    /// </summary>
    [SerializeField]
    [Tooltip("Clip")]
    private AudioClip _clip;

    /// <summary>
    ///   Delay time of clip play.
    /// </summary>
    [SerializeField]
    [Range(0.0F, 10.0F)]
    [Tooltip("Delay time of clip play")]
    private float _delay;

    #endregion

    #region Protected and private methods

    /// <summary>
    ///   Start.
    /// </summary>
    private void Start()
    {
      // Play clip.
      MusicManager.Instance.ClipPlay(_clip, _delay, true);
    }

    #endregion
  }
}