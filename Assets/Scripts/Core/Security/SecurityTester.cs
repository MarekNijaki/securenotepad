﻿using UnityEngine;

namespace N_Core.N_Security
{
	public class SecurityTester : MonoBehaviour
	{
		private void Awake()
		{
			// byte[] encryptedData = TripleDESAlgorithm.Encrypt("123456789", "Lubie jesc musli");
			// var decryptedData = TripleDESAlgorithm.Decrypt("123456789", encryptedData);
			// Debug.Log("Decrypted data = " + decryptedData);


			//SystemInfo.deviceUniqueIdentifier;
			// GetComponent<Text>().text = DataString;

			// this.GetComponent<Text>().text = "PlayerSettings.Android.keystoreName = " + PlayerSettings.Android.keystoreName + 
			//                                  "\n PlayerSettings.keystorePass = " +  PlayerSettings.keystorePass + 
			//                                  "\n PlayerSettings.Android.keyaliasName = " + PlayerSettings.Android.keyaliasName +
			//                                  "\n PlayerSettings.keyaliasPass = " + PlayerSettings.keyaliasPass;
			//    
			//    Debug.Log("PlayerSettings.Android.keystoreName = " + PlayerSettings.Android.keystoreName);
			//    Debug.Log("PlayerSettings.keystorePass = " + PlayerSettings.keystorePass);
			//    Debug.Log("PlayerSettings.Android.keyaliasName = " + PlayerSettings.Android.keyaliasName);
			//    Debug.Log("PlayerSettings.keyaliasPass = " + PlayerSettings.keyaliasPass);

			//PlayerSettings.Android.keystoreName = "asd";
			//PlayerSettings.keystorePass = "asd";
			//PlayerSettings.Android.keyaliasName = "asd";
			//PlayerSettings.keyaliasPass = "asd";

			// notepadpassword
			// 123456

			// var x = UnityEngine.Android
			//
			// PrivateKey key = ...; // Android KeyStore key
			//
			// KeyFactory factory = KeyFactory.getInstance(key.getAlgorithm(), "AndroidKeyStore");
			// KeyInfo keyInfo;
			// try {
			//  keyInfo = factory.getKeySpec(key, KeyInfo.class);
			// } catch (InvalidKeySpecException e) {
			//  // Not an Android KeyStore key.
			// }
		}


		void DisplayMetricsAndroid()
		{
			// Early out if we're not on an Android device
			if(Application.platform != RuntimePlatform.Android)
			{
				return;
			}

			// The following is equivalent to this Java code:
			//
			// metricsInstance = new DisplayMetrics();
			// UnityPlayer.currentActivity.getWindowManager().getDefaultDisplay().getMetrics(metricsInstance);
			//
			// ... which is pretty much equivalent to the code on this page:
			// http://developer.android.com/reference/android/util/DisplayMetrics.html

			// using(AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"), metricsClass = new AndroidJavaClass("android.util.DisplayMetrics"))
			// {
			//  using(AndroidJavaObject metricsInstance = new AndroidJavaObject("android.util.DisplayMetrics"),
			//                          activityInstance = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"),
			//                          windowManagerInstance = activityInstance.Call<AndroidJavaObject>("getWindowManager"),
			//                          displayInstance = windowManagerInstance.Call<AndroidJavaObject>("getDefaultDisplay"))
			//  {
			//   displayInstance.Call("getMetrics", metricsInstance);
			//   Density = metricsInstance.Get<float>("density");
			//   DensityDPI = metricsInstance.Get<int>("densityDpi");
			//   HeightPixels = metricsInstance.Get<int>("heightPixels");
			//   WidthPixels = metricsInstance.Get<int>("widthPixels");
			//   ScaledDensity = metricsInstance.Get<float>("scaledDensity");
			//   XDPI = metricsInstance.Get<float>("xdpi");
			//   YDPI = metricsInstance.Get<float>("ydpi");
			//  }
			// }
		}

		void X()
		{
			// Early out if we're not on an Android device
			if(Application.platform != RuntimePlatform.Android)
			{
				return;
			}

			// The following is equivalent to this Java code:
			//
			// metricsInstance = new DisplayMetrics();
			// UnityPlayer.currentActivity.getWindowManager().getDefaultDisplay().getMetrics(metricsInstance);
			//
			// ... which is pretty much equivalent to the code on this page:
			// http://developer.android.com/reference/android/util/DisplayMetrics.html

			// SharedPreferences playerPrefs = this.getSharedPreferences(this.getPackageName() + ".v2.playerprefs", Context.MODE_PRIVATE);
			// SharedPreferences.Editor editor = getSharedPreferences("preferenceName", MODE_PRIVATE).edit();
			// editor.putString("key", "value");
			// editor.commit();

			using(AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"), metricsClass = new AndroidJavaClass("android.util.DisplayMetrics"))
			{
				using(AndroidJavaObject myKeyInfo = new AndroidJavaObject("android.security.keystore.KeyInfo"))
				{
					DataString = myKeyInfo.ToString();
				}
			}
		}

		// The logical density of the display
		public static string DataString { get; protected set; }

		private void OtherTests()
		{
			//Crypto.
			//System.Security.Cryptography.
			
			//PlayerSettings.Android.keystoreName = "asd";
			//PlayerSettings.keystorePass = "asd";
			//PlayerSettings.keyaliasPass = "asd";
			//PlayerSettings.Android.keyaliasName = "asd";
			// PlayerSettings.Android.preferredInstallLocation = AndroidPreferredInstallLocation.ForceInternal;
			// //PlayerSettings.Android.useCustomKeystore = false;
			// PlayerSettings.Android.forceSDCardPermission = false;
			// PlayerSettings.Android.useAPKExpansionFiles = false;
		}
		
		private bool IsPasswordValid()
		{
			// // MN:TO_DO: add salt?
			// // Setup SHA algorithm.
			// SHA512Managed crypt = new SHA512Managed();
			// string hash = string.Empty;
			// // Compute hash.
			// byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(HASH_VALIDATION_STRING), 0, Encoding.UTF8.GetByteCount(HASH_VALIDATION_STRING));
			// // Convert to hexadecimal.
			// foreach(byte bit in crypto)
			// {
			// 	hash += bit.ToString("x2");
			// }
			//
			// // Return flag if password is valid.
			// return (hash == PlayerPrefs.GetString("hash"));

			return false;
		}
		
		private void GenerateHashFromSaveData()
		{
			// // MN:TO_DO: add salt?
			// // Setup SHA algorithm.
			// SHA512Managed crypt = new SHA512Managed();
			// string hash = string.Empty;
			// // Compute hash.
			// byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(HASH_VALIDATION_STRING), 0, Encoding.UTF8.GetByteCount(HASH_VALIDATION_STRING));
			// // Convert to hexadecimal.
			// foreach(byte bit in crypto)
			// {
			// 	hash += bit.ToString("x2");
			// }
			// // MN:TO_DO: can I save it in save data?
			// // Save hash.
			// PlayerPrefs.SetString("hash",hash);
		}

		
	}
}