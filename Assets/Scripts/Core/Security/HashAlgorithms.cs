﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace N_Core.N_Security
{
	/// <summary>
	///   Class storing hash algorithms.
	/// </summary>
	public static class HashAlgorithms
	{
		#region Public methods
		
		/// <summary>
		///   Compute HMAC hash using password as key.
		///   <para></para>
		///   Combining one way hash function with secret cryptographic key is called HMAC.
		///   <para></para>
		///   HMAC stands for 'Hash-based Message Authentication Code'.
		/// </summary>
		/// <param name="textToHash">Text to hash</param>
		/// <param name="password">Password to use as a key for hashing</param>
		/// <returns>Hashed text</returns>
		public static string ComputeHMACHashFromPassword(string textToHash, string password)
		{
			// Convert password into key.
			byte[] key = Encoding.UTF8.GetBytes(password);
			// Compute HMAC hash.
			byte[] hmac = ComputeHMACHash(Encoding.UTF8.GetBytes(textToHash), key);
			// Return hashed string.
			return Convert.ToBase64String(hmac);
		}

		/// <summary>
		///   Compute HMAC hash.
		///   <para></para>
		///   Combining one way hash function with secret cryptographic key is called HMAC.
		///   <para></para>
		///   HMAC stands for 'Hash-based Message Authentication Code'.
		/// </summary>
		/// <param name="textToHash">Text to hash</param>
		/// <param name="key"></param>
		/// <returns></returns>
		private static byte[] ComputeHMACHash(byte[] textToHash, byte[] key)
		{
			using(HMACSHA512 hmac = new HMACSHA512(key))
			{
				return hmac.ComputeHash(textToHash);
			}
		}
		
		#endregion
	}
}