﻿using System.Security.Cryptography;
using N_Core.N_Loggers;

namespace N_Core.N_Security
{
	/// <summary>
	///   Class used for string data associated with keys.
	/// </summary>
	public static class KeysContainer
	{
		/// <summary>
		///   Generate key and save it in container of given name.
		/// </summary>
		/// <param name="containerName">Container name</param>
		public static void GenerateKeyAndSavedItInContainer(string containerName)
		{
			// Create the CspParameters object and set the key container name used to store the RSA key pair.  
			new CspParameters { KeyContainerName = containerName };
		}
		
		/// <summary>
		///   Generate key.
		/// </summary>
		/// <param name="keySize">Key size (cannot be less than 8 bytes)</param>
		/// <returns>Generated key</returns>
		private static byte[] GenerateKey(int keySize = 32)
		{
			if(keySize <= 8)
			{
				CoreLogger.Instance.Error("Key size cannot be less than 8 bytes");
				return null;
			}
			
			using(RNGCryptoServiceProvider randomNumberGenerator = new RNGCryptoServiceProvider())
			{
				byte[] randomNumber = new byte[keySize];
				randomNumberGenerator.GetBytes(randomNumber);

				return randomNumber;
			}
		}

		/// <summary>
		///   Get key from container of given name.
		/// </summary>
		/// <param name="containerName">Container name</param>
		/// <returns>Key from container of given name</returns>
		public static string GetKey(string containerName)
		{
			// Create the CspParameters object and set the key container name used to store the RSA key pair.
			// 'ProviderType = 1' is used for RSA crypto service providers.
			CspParameters cp = new CspParameters { ProviderType = 1, KeyContainerName = containerName };
			
			// Create a new instance of RSACryptoServiceProvider that accesses the key container.  
			//RSACryptoServiceProvider.UseMachineKeyStore = false;
			RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(cp) { PersistKeyInCsp = true };
			
			// Return key.
			return rsa.ToXmlString(true);
		}

		/// <summary>
		///   Delete key from container of given name.
		/// </summary>
		/// <param name="containerName">Container from which to delete key</param>
		public static void DeleteKeyFromContainer(string containerName)
		{
			// Create the CspParameters object and set the key container name used to store the RSA key pair.  
			// 'ProviderType = 1' is used for RSA crypto service providers.
			CspParameters cp = new CspParameters { ProviderType = 1, KeyContainerName = containerName };
			
			// Create a new instance of RSACryptoServiceProvider that accesses the key container
			// and delete the key entry in the container.
			RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(cp) { PersistKeyInCsp = false };

			// Call Clear to release resources and delete the key from the container.  
			rsa.Clear();
		}
	}
}