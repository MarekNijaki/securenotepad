﻿using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

namespace N_Core.N_Security
{
	/// <summary>
	///   TripleDES algorithm.
	/// </summary>
	public static class TripleDESAlgorithm
	{
		#region Protected and private fields
		
		/// <summary>
		///   Number of password hashes to generate key for encryption and decryption algorithm.
		/// </summary>
		private const int NUMBER_OF_PASSWORD_HASHES_TO_GENERATE_KEY = 1000;

		/// <summary>
		///   Name of container storing initialization vector.
		/// </summary>
		private const string INITIALIZATION_VECTOR_CONTAINER_NAME = "InitializationVectorContainer";
		
		#endregion
		
		#region Public methods
		
		/// <summary>
		///   Encrypt data.
		/// </summary>
		/// <param name="password">Password to use for encryption</param>
		/// <param name="dataToEncrypt">Data to encrypt</param>
		/// <returns>Encrypted data</returns>
		public static byte[] Encrypt(string password, string dataToEncrypt)
		{
			// If invalid password then exit.
			if(string.IsNullOrEmpty(password) || (password.Length < 9))
			{
				Debug.Log("You must specify the password for encryption");
				return null;
			}

			// Generate salt.
			byte[] salt = GenerateSalt();
			// MN:TO_DO: fix this.
			// Save generated salt.
			PlayerPrefs.SetString("salt", ByteArrayConverter.ConvertByteArrayTo64String(salt));
			
			// Generate key for encryption.
			Rfc2898DeriveBytes encryptionKey = GenerateKeyForEncryption(password, salt);
			
			// Convert data to encrypt to UTF8 encoding.
			byte[] dataToEncryptInUTF8 = new UTF8Encoding(false).GetBytes(dataToEncrypt);
			
			// Create encryption algorithm and set configuration data for it.
			TripleDES tripleDes = TripleDES.Create();
			tripleDes.Key = encryptionKey.GetBytes(16);
			tripleDes.GenerateIV();
			
			// MN:TO_DO: fix this.
			// Save generated initialization vector.
			PlayerPrefs.SetString("iv", ByteArrayConverter.ConvertByteArrayTo64String(tripleDes.IV));
			
			// Create encryption stream.
			MemoryStream encryptionStream = new MemoryStream();
			CryptoStream encrypt = new CryptoStream(encryptionStream, tripleDes.CreateEncryptor(), CryptoStreamMode.Write);
			
			// Encrypt the data.
			encrypt.Write(dataToEncryptInUTF8, 0, dataToEncryptInUTF8.Length);
			encrypt.FlushFinalBlock();
			encrypt.Close();
			
			// Clear data.
			encryptionKey.Reset(); 
			encryptionKey.Dispose();
			tripleDes.Clear();
			tripleDes.Dispose();
			
			// Return encrypted data.
			return encryptionStream.ToArray();
		}
		
		/// <summary>
		///   Decrypt data.
		/// </summary>
		/// <param name="password">Password to use for decryption</param>
		/// <param name="dataToDecrypt">Data to decrypt</param>
		/// <returns>Decrypted data</returns>
		public static string Decrypt(string password, byte[] dataToDecrypt)
		{
			// If invalid password then exit.
			if(string.IsNullOrEmpty(password) || (password.Length < 9))
			{
				Debug.Log("You must specify the password for decryption");
				return null;
			}

			// MN:TO_DO: fix this.
			// Get salt.
			byte[] salt = ByteArrayConverter.Convert64StringToByteArray(PlayerPrefs.GetString("salt"));
			
			// Generate key for decryption.
			Rfc2898DeriveBytes decryptionKey = GenerateKeyForDecryption(password, salt);
			
			// Create decryption algorithm and set configuration data for it.
			TripleDES tripleDes = TripleDES.Create();
			tripleDes.Key = decryptionKey.GetBytes(16);
			// MN:TO_DO: fix this.
			tripleDes.IV = ByteArrayConverter.Convert64StringToByteArray(PlayerPrefs.GetString("iv"));
			
			// Create decryption stream.
			MemoryStream decryptionStreamBacking = new MemoryStream();
			CryptoStream decrypt = new CryptoStream(decryptionStreamBacking, tripleDes.CreateDecryptor(), CryptoStreamMode.Write);
			
			// Decrypt the data.
			decrypt.Write(dataToDecrypt, 0, dataToDecrypt.Length);
			decrypt.Flush();
			decrypt.Close();
			
			// Clear data.
			decryptionKey.Reset(); 
			decryptionKey.Dispose();
			tripleDes.Clear();
			tripleDes.Dispose();
			
			// Convert data to string encoded in UTF8.
			string decryptedData = new UTF8Encoding(false).GetString(decryptionStreamBacking.ToArray());
			
			// Return decrypted data.
			return decryptedData;
		}
		
		#endregion
		
		#region Protected and private methods
		
		/// <summary>
		///   Generate salt.
		/// </summary>
		/// <returns>Table that store salt</returns>
		private static byte[] GenerateSalt()
		{
			// Create a byte array to hold the random value. 
			byte[] salt = new byte[8];
			// Fill the array with a random value.
			using(RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
			{
				rngCsp.GetBytes(salt);
			}
			
			return salt;
		}

		/// <summary>
		///   Generate key for encryption.
		/// </summary>
		/// <param name="password">Password used to generate key</param>
		/// <param name="salt">Salt used for hashing of password</param>
		/// <returns>Key that can be used for encryption</returns>
		private static Rfc2898DeriveBytes GenerateKeyForEncryption(string password, byte[] salt)
		{
			return new Rfc2898DeriveBytes(password, salt, NUMBER_OF_PASSWORD_HASHES_TO_GENERATE_KEY);
		}
		
		/// <summary>
		///   Generate key for encryption.
		/// </summary>
		/// <param name="password">Password used to generate key</param>
		/// <param name="salt">Salt used for hashing of password</param>
		/// <returns>Key that can be used for decryption</returns>
		private static Rfc2898DeriveBytes GenerateKeyForDecryption(string password, byte[] salt)
		{
			return new Rfc2898DeriveBytes(password, salt);
		}
		
		/// <summary>
		///   Generate initialization vector from key stored in keys container.
		/// </summary>
		private static void GenerateIVFromKey()
		{
			// MN:TO_DO: Method not in use because 'KeysContainer.GetKey' throws error on android studio associated with
			// trying accessing forbidden memory. 
			
			// MN:TO_DO: IF YOU WANT TO USE THIS METHOD, THEN ADD CODE FOR EXTRACTING KEY FROM XML TAGS IN STRING 
			// OBTAINED FROM 'KeysContainer.GetKey(...)'.
			
			// If key exist.
			if(!string.IsNullOrEmpty(KeysContainer.GetKey(INITIALIZATION_VECTOR_CONTAINER_NAME)))
			{
				// Delete existing key.
				KeysContainer.DeleteKeyFromContainer(INITIALIZATION_VECTOR_CONTAINER_NAME);
			}
			
			// Generate new key.
			KeysContainer.GenerateKeyAndSavedItInContainer(INITIALIZATION_VECTOR_CONTAINER_NAME);
		}
		
		/// <summary>
		///   Get initialization vector from key stored in keys container.
		/// </summary>
		/// <param name="blockSize">Block size of algorithm</param>
		/// <returns>Initialization vector</returns>
		private static byte[] GetIVFromKey(int blockSize = 8)
		{
			// MN:TO_DO: Method not in use because 'KeysContainer.GetKey' throws error on android studio associated with
			// trying accessing forbidden memory. 
			
			// MN:TO_DO: IF YOU WANT TO USE THIS METHOD, THEN ADD CODE FOR EXTRACTING KEY FROM XML TAGS IN STRING 
			// OBTAINED FROM 'KeysContainer.GetKey(...)'.
			
			// Get saved initialization vector hash.
			string savedIVHash = KeysContainer.GetKey(INITIALIZATION_VECTOR_CONTAINER_NAME);
			// Convert saved initialization hash to byte array.
			byte[] savedIV = new UTF8Encoding(false).GetBytes(savedIVHash);
			// Extract initialization vector from hash.
			byte[] iv = new byte[blockSize];
			for(int i = 0; i < blockSize; i++)
			{
				iv[i] = savedIV[i];
			}

			return iv;
		}
		
		#endregion
	}
}