﻿using System.IO;
using System.Security.Cryptography;
using N_Core.N_Loggers;

namespace N_Core.N_Security
{
	/// <summary>
	///   AES algorithm.
	/// </summary>
	public static class AesAlgorithm
	{
		#region Protected and private fields

		/// <summary>
		///   Temporary saved key (will be cleaned after exit from application).
		/// </summary>
		private static byte[] _tmpSavedKey;
		
		/// <summary>
		///   Temporary saved initialization vector (will be cleaned after exit from application).
		/// </summary>
		private static byte[] _tmpSavedIV;
		
		#endregion
		
		#region Public methods
		
		/// <summary>
		///   Encrypt data.
		/// </summary>
		/// <param name="dataToEncrypt">Data to encrypt</param>
		/// <returns>Encrypted data</returns>
		public static byte[] Encrypt(string dataToEncrypt)
		{
			// Encrypted data.
			byte[] encryptedData;
			
			// Create a new instance of the AesCryptoServiceProvider class.
			// This generates a new key and initialization vector (IV).
			using(AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
			{
				// Encrypt the string to an array of bytes.
				encryptedData = EncryptWithAes(dataToEncrypt, aes.Key, aes.IV);
				
				// MN:TO_DO: fix this (store this in more secure and permanent container).
				// Save key and initialization vector.
				_tmpSavedKey = aes.Key;
				_tmpSavedIV = aes.IV;
			}
				
			// Return encrypted data.
			return encryptedData;
		}
		
		/// <summary>
		///   Decrypt data.
		/// </summary>
		/// <param name="dataToDecrypt">Data to decrypt</param>
		/// <returns>Decrypted data</returns>
		public static string Decrypt(byte[] dataToDecrypt)
		{
			// Decrypted data.
			string decryptedData;
			
			// Create a new instance of the AesCryptoServiceProvider class.
			using(AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
			{
				// MN:TO_DO: fix this (store this in more secure and permanent container).
				// Set key and initialization vector.
				aes.Key = _tmpSavedKey;
				aes.IV = _tmpSavedIV;

				// Decrypt the bytes to a string.
				decryptedData = DecryptWithAes(dataToDecrypt, aes.Key, aes.IV);
			}

			// Return encrypted data.
			return decryptedData;
		}
		
		#endregion

		#region Protected and private methods
		
		/// <summary>
		///   Encrypt data using AES algorithm.
		/// </summary>
		/// <param name="dataToEncrypt">Data to encrypt</param>
		/// <param name="key">Key to use for algorithm</param>
		/// <param name="iv">Initialization vector to use for algorithm</param>
		/// <returns>Encrypted data</returns>
		private static byte[] EncryptWithAes(string dataToEncrypt, byte[] key, byte[] iv)
		{
			// If data to encrypt is null then log message and exit.
			if(string.IsNullOrEmpty(dataToEncrypt))
			{
				CoreLogger.Instance.Error("Data to encrypt cannot be null or empty");
				return null;
			}
			
			// If key is null then log message and exit.
			if((key == null) || (key.Length <= 0))
			{
				CoreLogger.Instance.Error("Key cannot be null or empty");
				return null;
			}
			
			// If initialization vector is null then log message and exit.
			if((iv == null) || (iv.Length <= 0))
			{
				CoreLogger.Instance.Error("Initialization vector cannot be null or empty");
				return null;
			}
			
			// Encrypted data.
			byte[] encryptedData;
			// Create an AesCryptoServiceProvider object with the specified key and IV.
			using(AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
			{
				// Set key and initialization vector.
				aes.Key = key;
				aes.IV = iv;

				// Create an encryptor to perform the stream transform.
				ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

				// Create the streams used for encryption.
				using(MemoryStream msEncrypt = new MemoryStream())
				{
					using(CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
					{
						using(StreamWriter swEncrypt = new StreamWriter(csEncrypt))
						{
							// Write all data to the stream.
							swEncrypt.Write(dataToEncrypt);
						}
						
						// Save encrypted data.
						encryptedData = msEncrypt.ToArray();
					}
				}
			}

			// Return encrypted data.
			return encryptedData;
		}

		/// <summary>
		///   Decrypt data using AES algorithm.
		/// </summary>
		/// <param name="dataToDecrypt">Data to decrypt</param>
		/// <param name="key">Key to use for algorithm</param>
		/// <param name="iv">Initialization vector to use for algorithm</param>
		/// <returns>Decrypted data</returns>
		private static string DecryptWithAes(byte[] dataToDecrypt, byte[] key, byte[] iv)
		{
			// If data to decrypt is null then log message and exit.
			if((dataToDecrypt == null) || (dataToDecrypt.Length <= 0))
			{
				CoreLogger.Instance.Error("Data to decrypt cannot be null or empty");
				return null;
			}
			
			// If key is null then log message and exit.
			if((key == null) || (key.Length <= 0))
			{
				CoreLogger.Instance.Error("Key cannot be null or empty");
				return null;
			}
			
			// If initialization vector is null then log message and exit.
			if((iv == null) || (iv.Length <= 0))
			{
				CoreLogger.Instance.Error("Initialization vector cannot be null or empty");
				return null;
			}
			
			// Decrypted data.
			string decryptedData;
			// Create an AesCryptoServiceProvider object with the specified key and IV.
			using(AesCryptoServiceProvider aes = new AesCryptoServiceProvider())
			{
				// Set key and initialization vector.
				aes.Key = key;
				aes.IV = iv;

				// Create a decryptor to perform the stream transform.
				ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

				// Create the streams used for decryption.
				using(MemoryStream msDecrypt = new MemoryStream(dataToDecrypt))
				{
					using(CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
					{
						using(StreamReader srDecrypt = new StreamReader(csDecrypt))
						{
							// Read the decrypted bytes from the decrypting stream and place them in a string.
							decryptedData = srDecrypt.ReadToEnd();
						}
					}
				}
			}

			// Return decrypted data.
			return decryptedData;
		}
		
		#endregion
	}
}