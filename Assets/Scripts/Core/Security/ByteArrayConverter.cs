﻿using System;
using System.Text;

namespace N_Core.N_Security
{
	/// <summary>
	///   Class used for conversion between byte array and other types.
	/// </summary>
	public static class ByteArrayConverter
	{
		#region Public methods

		/// <summary>
		///   Convert byte array to string.
		/// </summary>
		/// <param name="bytes">Byte array to convert to string</param>
		/// <returns>String converted from byte array</returns>
		public static string ConvertByteArrayTo64String(byte[] bytes)
		{
			return Convert.ToBase64String(bytes);
		}

		/// <summary>
		///   Convert byte array to string.
		/// </summary>
		/// <param name="bytes">Byte array to convert to string</param>
		/// <returns>String converted from byte array</returns>
		public static string ConvertByteArrayToString(byte[] bytes)
		{
			char[] chars = new char[bytes.Length / sizeof(char)];
			Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new string(chars);
		}

		/// <summary>
		///   Convert byte array to string.
		/// </summary>
		/// <param name="bytes">Byte array to convert to string</param>
		/// <returns>String converted from byte array</returns>
		private static string ConvertByteArrayToUTF8String(byte[] bytes)
		{
			return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
		}

		/// <summary>
		///   Convert string to byte array.
		/// </summary>
		/// <param name="str">String to convert to byte array</param>
		/// <returns>Byte array converted from string</returns>
		public static byte[] Convert64StringToByteArray(string str)
		{
			return Convert.FromBase64String(str);
		}

		/// <summary>
		///   Convert string to byte array.
		/// </summary>
		/// <param name="str">String to convert to byte array</param>
		/// <returns>Byte array converted from string</returns>
		public static byte[] ConvertStringToByteArray(string str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		/// <summary>
		///   Convert string to byte array.
		/// </summary>
		/// <param name="str">String to convert to byte array</param>
		/// <returns>Byte array converted from string</returns>
		private static byte[] ConvertUTF8StringToByteArray(string str)
		{
			return Encoding.UTF8.GetBytes(str);
		}

		#endregion
	}
}