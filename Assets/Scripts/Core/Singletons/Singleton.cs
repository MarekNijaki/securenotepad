﻿using System;
using System.Reflection;
using N_Core.N_Loggers;

namespace N_Core.N_Singletons
{
	/// <summary>
	///   Generic singleton.
	/// </summary>
	/// <typeparam name="T">Constraint only to classes(reference types)</typeparam>
	public abstract class Singleton<T> where T : class
	{
		#region Public fields

		/// <summary>
		///   Instance of singleton.
		/// </summary>
		public static T Instance
		{
			get
			{
				if(_instance == null)
				{
					BaseInit();
				}
				return _instance;
			}
		}

		#endregion

		#region Protected and private fields

		/// <summary>
		///   Instance of singleton.
		/// </summary>
		private static T _instance;

		/// <summary>
		///   Dummy object only for thread safety.
		/// </summary>
		private static readonly object _threadLock = new object();

		#endregion

		#region Protected and private methods

		/// <summary>
		///   Base initialization.
		/// </summary>
		private static void BaseInit()
		{
			// Lock operation for thread safety.
			lock(_threadLock)
			{
				// If there is instance then exit.
				if(_instance != null)
				{
					CoreLogger.Instance.SingletonError($"Cannot instantiate singleton [{typeof(T)}] because other instance of it already exist");
					return;
				}
				// Get type of singelton.
				Type singeltonType = typeof(T);
				// Get public constructors. 
				ConstructorInfo[] publicConstructors = singeltonType.GetConstructors();
				// If there are public constructors then exit.
				if(publicConstructors.Length > 0)
				{
					CoreLogger.Instance.SingletonError($"Class [{singeltonType.Name}] has at least one public constructor, making it impossible to enforce singleton behaviour");
					return;
				}
				// Create instance using private constructor.
				_instance = (T)Activator.CreateInstance(singeltonType, true);
			}
		}

		#endregion
	}
}