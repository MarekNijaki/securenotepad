﻿using N_Core.N_Loggers;
using UnityEngine;

namespace N_Core.N_Singletons
{
	/// <summary>
	///   Generic Unity singleton (last through scenes).
	/// </summary>
	/// <typeparam name="T">Class inherited from 'MonoBehaviour' with constraint to component (and all component subclasses)</typeparam>
	public abstract class UnitySingleton<T> : MonoBehaviour where T : Component
	{
		#region Public fields

		/// <summary>
		///   Instance of singleton.
		/// </summary>
		public static T Instance { get; private set; }

		#endregion

		#region Protected and private methods

		/// <summary>
		///   Initialization.
		/// </summary>
		protected virtual void Init()
		{
		}

		/// <summary>
		///   Event - on destroy.
		/// </summary>
		protected virtual void OnDestroy()
		{
			// If not singleton instance then exit.
			if(this != Instance)
			{
				return;
			}
			// Clean singleton before destroying.
			CleanBeforeDestroy();
		}

		/// <summary>
		///   Clean singleton before destroying.
		/// </summary>
		protected virtual void CleanBeforeDestroy()
		{
			Instance = null;
		}
		
		/// <summary>
		///   Awake.
		/// </summary>
		private void Awake()
		{
			if(Instance == null)
			{
				BaseInit();
			}
			else
			{
				if(Instance == this)
				{
					return;
				}
				
				// Log message.
				CoreLogger.Instance.SingletonError($"Destroyed obsolete(additional) singleton [{typeof(T)}]");
				// Destroy current(redundant) object.
				Destroy(gameObject);
			}
		}

		/// <summary>
		///   Base initialization.
		/// </summary>
		private void BaseInit()
		{
			// Get singleton from component.
			Instance = GetComponent<T>();
			if(Instance == null)
			{
				CoreLogger.Instance.SingletonError($"Initialization of singleton [{typeof(T)}] failed");
				return;
			}
			// Find root game object (parent without parent).
			Transform root = transform;
			while(root.parent != null)
			{
				root = root.parent;
			}
			// Set root game object to not destroy when loading a new scene.
			DontDestroyOnLoad(root.gameObject);
			// Run additional initialization.
			Init();
		}

		#endregion
	}
}