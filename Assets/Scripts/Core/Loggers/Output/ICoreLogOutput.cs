﻿using UnityEngine;

namespace N_Core.N_Loggers.Output
{
	/// <summary>
	///   Interface that determine where output messages will be put.
	/// </summary>
	public interface ICoreLogOutput
	{
		#region Public methods
		
		/// <summary>
		///   Method used for putting message into the output.
		///   <para></para>
		///   All messages can be filtered using parameters.
		/// </summary>
		/// <param name="message">Message that will be logged</param>
		/// <param name="categoryIndex">Index of category</param>
		/// <param name="coreLogType">Log type of the message</param>
		/// <param name="context">Object context</param>
		void Log(object message, int categoryIndex, CoreLogType coreLogType, Object context);

		/// <summary>
		///   Method used for putting message into the output.
		///   <para></para>
		///   All messages can be filtered using parameters.
		/// </summary>
		/// <param name="message">Message that will be logged</param>
		/// <param name="categoryIndex">Index of category</param>
		/// <param name="coreLogType">Log type of the message</param>
		void Log(object message, int categoryIndex, CoreLogType coreLogType);
		
		#endregion
	}
}