﻿using UnityEngine;

namespace N_Core.N_Loggers.Output
{
	/// <summary>
	///   Class that manage logging information to ConsolePRO.
	/// </summary>
	public class ConsoleProLoggerOutput : ICoreLogOutput
	{
		#region Public methods
		
		/// <summary>
		///   Method used for putting message into the output.
		///   <para></para>
		///   All messages can be filtered using parameters.
		/// </summary>
		/// <param name="message">Message that will be logged</param>
		/// <param name="categoryIndex">Index of category</param>
		/// <param name="coreLogType">Log type of the message</param>
		/// <param name="context">Object context</param>
		public void Log(object message, int categoryIndex, CoreLogType coreLogType, Object context)
		{
			if(coreLogType == CoreLogType.Error)
			{
				Debug.LogError((string)message);
			}
			else
			{
				if(categoryIndex > -1)
				{
					ConsoleProDebug.LogToFilter((string)message, CoreBaseLogger.GetCategory(categoryIndex));
				}
				else
				{
					Debug.Log(message);
				}
			}
		}

		/// <summary>
		///   Method used for putting message into the output.
		///   <para></para>
		///   All messages can be filtered using parameters.
		/// </summary>
		/// <param name="message">Message that will be logged</param>
		/// <param name="categoryIndex">Index of category</param>
		/// <param name="coreLogType">Log type of the message</param>
		public void Log(object message, int categoryIndex, CoreLogType coreLogType)
		{
			if(coreLogType == CoreLogType.Error)
			{
				Debug.LogError((string)message);
			}
			else
			{
				if(categoryIndex > -1)
				{
					ConsoleProDebug.LogToFilter((string)message, CoreBaseLogger.GetCategory(categoryIndex));
				}
				else
				{
					Debug.Log(message);
				}
			}
		}
		
		#endregion
	}
}