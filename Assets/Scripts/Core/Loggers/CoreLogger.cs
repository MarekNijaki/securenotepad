﻿namespace N_Core.N_Loggers
{
	/// <summary>
	///   Core logger.
	/// </summary>
	public class CoreLogger : CoreBaseLogger
	{
		#region Protected and private fields

		/// <summary>
		///   Single static instance.
		/// </summary>
		private static CoreLogger _instance;

		#endregion

		#region Public fields

		/// <summary>
		///   Single static instance.
		/// </summary>
		public static CoreLogger Instance
		{
			get
			{
				if(_instance == null)
				{
					_instance = new CoreLogger();
				}

				return _instance;
			}
		}

		#endregion
	}
}