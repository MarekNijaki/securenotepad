﻿using System;

namespace N_Core.N_Loggers
{
	/// <summary>
	///   Class that manages logging information.
	/// </summary>
	public abstract class CoreBaseLogger
	{
		#region Protected and private fields

		/// <summary>
		///   List of default log categories.
		/// </summary>
		private static readonly string[] defCategories =
		{
			"Info",
			"Warning",
			"Error",
			"SingletonInfo",
			"SingletonWarning",
			"SingletonError",
			"GameManagementInfo",
			"GameManagementWarning",
			"GameManagementError",
			"LevelsManagementInfo",
			"LevelsManagementWarning",
			"LevelsManagementError",
			"SoundManagementInfo",
			"SoundManagementWarning",
			"SoundManagementError",
			"PlayerSettingsInfo",
			"PlayerSettingsWarning",
			"PlayerSettingsError"
		};

		/// <summary>
		///   List of log categories.
		/// </summary>
		private static string[] categories;

		#endregion

		#region Public methods

		/// <summary>
		///   Get category of given index.
		/// </summary>
		/// <param name="categoryIndex">Index of category</param>
		/// <returns>Category of given index</returns>
		/// <exception cref="ArgumentException">Exception to throw if categories array is null or empty</exception>
		/// <exception cref="ArgumentOutOfRangeException">Exception to throw if index of category is out of range</exception>
		public static string GetCategory(int categoryIndex)
		{
			if((categories == null) || (categories.Length == 0))
			{
				categories = defCategories.Clone() as string[];
			}

			if(categories == null)
			{
				throw new ArgumentException("Cannot get category from empty collection");
			}

			if((categoryIndex < 0) && (categoryIndex >= categories.Length))
			{
				throw new ArgumentOutOfRangeException(nameof(categoryIndex));
			}

			return categories[categoryIndex];
		}

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void Info(string message)
		{
			Log(message, 0);
		}

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void Warning(string message)
		{
			Log(message, 1);
		}

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void Error(string message)
		{
			Log(message, 2);
		}

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void SingletonInfo(string message)
		{
			Log(message, 3);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void SingletonWarning(string message)
		{
			Log(message, 4);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void SingletonError(string message)
		{
			Log(message, 5);
		}

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void GameManagementInfo(string message)
		{
			Log(message, 6);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void GameManagementWarning(string message)
		{
			Log(message, 7);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void GameManagementError(string message)
		{
			Log(message, 8);
		}

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void LevelsManagementInfo(string message)
		{
			Log(message, 9);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void LevelsManagementWarning(string message)
		{
			Log(message, 10);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void LevelsManagementError(string message)
		{
			Log(message, 11);
		}

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void SoundManagementInfo(string message)
		{
			Log(message, 12);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void SoundManagementWarning(string message)
		{
			Log(message, 13);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void SoundManagementError(string message)
		{
			Log(message, 14);
		}

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void PlayerSettingsInfo(string message)
		{
			Log(message, 15);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void PlayerSettingsWarning(string message)
		{
			Log(message, 16);
		}
		
		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		public void PlayerSettingsError(string message)
		{
			Log(message, 17);
		}

		#endregion
		
		#region Protected and private methods

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		/// <param name="categoryIndex">Index of category</param>
		protected static void LogFromProject(string message, int categoryIndex)
		{
			Log(message, defCategories.Length + categoryIndex);
		}

		/// <summary>
		///   Add new categories.
		/// </summary>
		/// <param name="additionalValues">Array of additional values</param>
		/// <exception cref="ArgumentException">Exception to throw if additional values array is null or empty</exception>
		protected static void AddCategories(params string[] additionalValues)
		{
			// With params you can call your method like this:
			// AddCategories("a", "b", "c");
			// Without params, you can’t. You should use then AddCategories(new string[] { "a", "b", "c" });

			if(additionalValues == null)
			{
				throw new ArgumentException("Passed array is null");
			}

			if(additionalValues.Length == 0)
			{
				throw new ArgumentException("Passed array is empty");
			}

			categories = new string[defCategories.Length + additionalValues.Length];
			defCategories.CopyTo(categories, 0);
			additionalValues.CopyTo(categories, defCategories.Length);
		}

		/// <summary>
		///   Log message to ConsolePRO.
		/// </summary>
		/// <param name="message">Message to log</param>
		/// <param name="filterName">Filter name(category)</param>
		private static void LogToConsolePro(string message, string filterName)
		{
			ConsoleProDebug.LogToFilter(message, filterName);
		}

		/// <summary>
		///   Log message.
		/// </summary>
		/// <param name="message">Message to log</param>
		/// <param name="categoryIndex">Index of category</param>
		private static void Log(string message, int categoryIndex)
		{
			LogToConsolePro(message, GetCategory(categoryIndex));
		}

		#endregion
	}
}