﻿using System;

namespace N_Core.N_Loggers
{
	/// <summary>
	///   Log types.
	/// </summary>
	[Flags]
	public enum CoreLogType
	{
		// The [Flags] attribute is used whenever the enumerable represents a collection of possible values, rather than a single value.
		// Such collections are often used with bitwise operators.
		// Bitwise values should be power of 2 so join values values (eg. 'All') are computed correctly.
		// The [Flags] attribute doesn't enable bitwise operators by itself. All it does, is allowing a nice representation by the '.ToString()' method, eg:
		// var str1 = (LogType.Info | LogType.Warning).ToString(); will output '3' without [Flags] attribute.
		// var str2 = (LogType.Info | LogType.Warning).ToString(); will output 'Info,Warning' with [Flags] attribute.

		/// <summary>
		///   No message.
		/// </summary>
		None = 0,
		/// <summary>
		///   Informing messages.
		/// </summary>
		Info = 1,
		/// <summary>
		///   Warning messages.
		/// </summary>
		Warning = 2,
		/// <summary>
		///   Error messages.
		/// </summary>
		Error = 4,
		/// <summary>
		///   All messages.
		///   <para></para>
		///   Bitwise value of 0x00000007.
		/// </summary>
		All = Error | Warning | Info
	}
}