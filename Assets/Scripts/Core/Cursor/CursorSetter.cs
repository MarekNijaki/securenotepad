﻿using UnityEngine;

namespace N_Core.N_Cursor
{
	/// <summary>
	///   Cursor setter.
	/// </summary>
	public class CursorSetter : MonoBehaviour
	{
		#region Serialized fields

		/// <summary>
		///   The texture that's going to replace the current cursor.
		/// </summary>
		[SerializeField]
		[Tooltip("The texture that's going to replace the current cursor")]
		private Texture2D _texture;

		#endregion

		#region Protected and private methods

		/// <summary>
		///   Awake.
		/// </summary>
		private void Awake()
		{
			Cursor.SetCursor(_texture, Vector2.zero, CursorMode.Auto);
		}

		#endregion
	}
}