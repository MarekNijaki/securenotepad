﻿using UnityEngine;

namespace N_Core.N_Utils
{
  /// <summary>
  ///   Don't destroy game object after loading next scene.
  /// </summary>
  public class DontDestroyOnLoad : MonoBehaviour
  {
    #region Protected and private methods

    /// <summary>
    ///   Start.
    /// </summary>
    private void Start()
    {
      // Make sure that game object will not be destroyed after loading next scene.
      DontDestroyOnLoad(gameObject);
    }

    #endregion
  }
}