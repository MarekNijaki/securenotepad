﻿using UnityEngine;

namespace N_Core.N_Levels
{
	/// <summary>
	///   Automatic changer of level.
	/// </summary>
	public class LevelAutoChanger : MonoBehaviour
	{
		#region Serialized fields

		/// <summary>
		///   Scene to load.
		/// </summary>
		[SerializeField]
		[Tooltip("Scene to load")]
		public LevelManager<LevelManager>.Scenes _scene;
		
		/// <summary>
		///   Delay of loading scene.
		/// </summary>
		[SerializeField]
		[Range(0.0F,15.0F)]
		[Tooltip("Delay of loading scene")]
		private float _delay;

		#endregion
		
		#region Public methods

		/// <summary>
		///   Quit.
		/// </summary>
		public void Quit()
		{
			LevelManager.Instance.Quit();
		}

		/// <summary>
		///   Load splash scene.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		public void SplashLoad(float delay)
		{
			LevelManager.Instance.SplashLoad(delay);
		}

		/// <summary>
		///   Load authorization scene.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		public void AuthLoad(float delay)
		{
			LevelManager.Instance.AuthLoad(delay);
		}

		/// <summary>
		///   Load main menu scene.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		public void MainMenuLoad(float delay)
		{
			LevelManager.Instance.MainMenuLoad(delay);
		}
		
		/// <summary>
		///   Load game scene.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		public void GameLoad(float delay)
		{
			LevelManager.Instance.GameLoad(delay);
		}

		/// <summary>
		///   Load next level.
		/// </summary>
		public void LvlLoadNext()
		{
			LevelManager.Instance.LvlLoadNext(0.0F,true);
		}

		#endregion
		
		#region Protected and private methods

		/// <summary>
		///   Start.
		/// </summary>
		private void Start()
		{
			// Load scene.
			LevelManager.Instance.SceneLoad(_scene,_delay);
		}

		#endregion
	}
}