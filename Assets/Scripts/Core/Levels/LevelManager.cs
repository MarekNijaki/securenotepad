﻿using System;
using System.Collections;
using N_Core.N_Singletons;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace N_Core.N_Levels
{
	/// <summary>
	///   Class that manage levels and scenes.
	/// </summary>
	public abstract class LevelManager<TInstance> : UnitySingleton<TInstance> where TInstance : Component
	{
		#region Events
		
		/// <summary>
		///   Event - fired after quit sequence started.
		/// </summary>
		public event Action QuitSequenceStarted;
		
		#endregion
		
		#region Public fields

		/// <summary>
		///   Enumeration of scenes.
		/// </summary>
		public enum Scenes { NONE, SPLASH, AUTH, MAIN_MENU, Game };
		
		/// <summary>
		///   Enumeration of levels.
		/// </summary>
		public enum Lvls { NONE }

		#endregion
		
		#region Protected and private fields

		/// <summary>
		///   Current scene.
		/// </summary>
		private Scenes _currentScene = Scenes.NONE;
		
		/// <summary>
		///   Current level.
		/// </summary>
		private Lvls _currentLevel = Lvls.NONE;
		
		/// <summary>
		///   Loading panel.
		/// </summary>
		private GameObject _loadingPanel;
		
		/// <summary>
		///   Progress slider.
		/// </summary>
		private Slider _progressSlider;

		#endregion

		#region Public methods
		
		/// <summary>
		///   Get current scene.
		/// </summary>
		/// <returns>Current scene</returns>
		public Scenes CurSceneGet()
		{
			return _currentScene;
		}

		/// <summary>
		///   Get current level.
		/// </summary>
		/// <returns>Current level</returns>
		public Lvls CurLvlGet()
		{
			return _currentLevel;
		}

		/// <summary>
		///   Quit.
		/// </summary>
		public void Quit()
		{
			QuitSequenceStarted?.Invoke();
			
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
      Application.Quit();
		#endif
		}

		/// <summary>
		///   Load splash scene.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		public void SplashLoad(float delay)
		{
			SceneLoad(Scenes.SPLASH,delay);
		}

		/// <summary>
		///   Load authorization scene.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		public void AuthLoad(float delay)
		{
			SceneLoad(Scenes.AUTH,delay);
		}
		
		/// <summary>
		///   Load main menu scene.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		public void MainMenuLoad(float delay)
		{
			SceneLoad(Scenes.MAIN_MENU,delay);
		}

		/// <summary>
		///   Load game scene.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		public void GameLoad(float delay)
		{
			SceneLoad(Scenes.Game,delay);
		}

		/// <summary>
		///   Load scene.
		/// </summary>
		/// <param name="scene">Scene to load</param>
		/// <param name="delay">Delay of loading scene</param>
		public void SceneLoad(Scenes scene, float delay)
		{
			// Load scene with delay.
			StartCoroutine(SceneLoadWithDelay(scene,delay));
		} // End of SceneLoad

		/// <summary>
		///   Load given level.
		/// </summary>
		/// <param name="level">Level to load</param>
		/// <param name="delay">Delay of loading scene</param>
		/// <param name="is_async">Flag if loading should be done asynchronous</param>
		public void LvlLoad(Lvls level, float delay, bool is_async)
		{
			_currentLevel = level;
			LvlReload(delay,is_async);
		}
		
		/// <summary>
		///   Load next level.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		/// <param name="is_async">Flag if loading should be done asynchronous</param>
		public void LvlLoadNext(float delay, bool is_async)
		{
			// Load next level with delay.
			StartCoroutine(LvlLoadNextWithDelay(delay,is_async));
		}

		/// <summary>
		///   Reload level.
		/// </summary>
		/// <param name="delay">Delay of loading scene</param>
		/// <param name="is_async">Flag if loading should be done asynchronous</param>
		public void LvlReload(float delay, bool is_async)
		{
			// Load next level with delay.
			StartCoroutine(LvlReloadWithDelay(delay,is_async));
		}
		
		#endregion
		
		#region Protected and private methods
		
		/// <summary>
		///   Start.
		/// </summary>
	  private void Start()
	  {
	    // Get loading panel.
	    _loadingPanel=GameObject.FindGameObjectWithTag("loading_screen").GetComponentsInChildren<Transform>(true)[1].gameObject;
	    // Get progress slider.
	    _progressSlider=_loadingPanel.GetComponentInChildren<Slider>();
	  }

	  /// <summary>
	  ///   Load scene with delay.
	  /// </summary>
	  /// <param name="scene">Scene to load</param>
	  /// <param name="delay">Delay of loading scene</param>
	  /// <returns>IEnumerator for coroutine</returns>
	  private IEnumerator SceneLoadWithDelay(Scenes scene, float delay)
	  {
	    // If no scene then exit from function.
	    if(scene==Scenes.NONE)
	    {
	      yield break;
	    }
	    // Wait for 'time' seconds.
	    yield return new WaitForSecondsRealtime(delay);
	    // Set current scene.
	    _currentScene=scene;
	    // Actualize current level.
	    _currentLevel=Lvls.NONE;
	    // Load scene.
	    SceneManager.LoadScene(SceneNameDecode(scene));
	  }

	  /// <summary>
	  ///   Load next level with delay.
	  /// </summary>
	  /// <param name="delay">Delay of loading scene</param>
	  /// <param name="is_async">Flag if loading should be done asynchronous</param>
	  /// <returns>IEnumerator for coroutine</returns>
	  private IEnumerator LvlLoadNextWithDelay(float delay, bool is_async)
	  {
	    // If current level is last.
	    if((int)_currentLevel==(Enum.GetNames(typeof(Lvls)).Length-1))
	    {
	      // Load 'Win' level.
	      //WinLoad(delay);
	      // Exit from function.
	      yield break;
	    }
	    // Wait for 'time' seconds.
	    yield return new WaitForSecondsRealtime(delay);
	    // Set current scene.
	    _currentScene=Scenes.NONE;
	    // Actualize current level.
	    _currentLevel++;
	    // If asynchronous mode.
	    if(is_async)
	    {
	      // Load next level.
	      AsyncOperation oper = SceneManager.LoadSceneAsync(LvlNameDecode(_currentLevel));
	      // Activate loading panel.
	      _loadingPanel.SetActive(true);
	      // Reset slider progress.
	      _progressSlider.value=0.0F;
	      // Duration of operation
	      float duration = Time.realtimeSinceStartup;
	      // Loop until operation is done.
	      while(!oper.isDone)
	      {
	        // Compute real progress (Unity always reserve '0.1' for activation stage, so it's better to omit that value).  
	        float progress = Mathf.Clamp01(oper.progress/0.9F);
	        // Change slider progress.
	        _progressSlider.value=progress;
	        // Yield until next frame.
	        yield return null;
	      }
	      // Compute duration of operation.
	      duration = Time.realtimeSinceStartup - duration;
	      // If duration of operation was shorter than 1 second.
	      if(duration < 1.0F)
	      {
	        // Yield to at least 1 full second.
	        yield return new WaitForSecondsRealtime(1.0F - duration);
	      }
	      // Disable loading panel.
	      _loadingPanel.SetActive(false);
	    }
	    // If not asynchronous mode.
	    else
	    {
	      // Load next level.
	      SceneManager.LoadScene(LvlNameDecode(_currentLevel));
	    }
	  }

	  /// <summary>
	  ///   Reload current level with delay.
	  /// </summary>
	  /// <param name="delay">Delay of loading scene</param>
	  /// <param name="is_async">Flag if loading should be done asynchronous</param>
	  /// <returns>IEnumerator for coroutine</returns>
	  private IEnumerator LvlReloadWithDelay(float delay,bool is_async)
	  {
	    // Wait for 'time' seconds.
	    yield return new WaitForSecondsRealtime(delay);
	    // If asynchronous mode.
	    if(is_async)
	    {
	      // Load level.
	      AsyncOperation oper = SceneManager.LoadSceneAsync(LvlNameDecode(_currentLevel));
	      // Activate loading panel.
	      _loadingPanel.SetActive(true);
	      // Reset slider progress.
	      _progressSlider.value=0.0F;
	      // Duration of operation
	      float duration = Time.realtimeSinceStartup;
	      // Loop until operation is done.
	      while(!oper.isDone)
	      {
	        // Compute real progress (Unity always reserve '0.1' for activation stage, so it's better to omit that value).  
	        float progress = Mathf.Clamp01(oper.progress/0.9F);
	        // Change slider progress.
	        _progressSlider.value=progress;
	        // Yield until next frame.
	        yield return null;
	      }
	      // Compute duration of operation.
	      duration = Time.realtimeSinceStartup - duration;
	      // If duration of operation was shorter than 1 second.
	      if(duration < 1.0F)
	      {
	        // Yield to at least 1 full second.
	        yield return new WaitForSecondsRealtime(1.0F - duration);
	      }
	      // Disable loading panel.
	      _loadingPanel.SetActive(false);
	    }
	    // If not asynchronous mode.
	    else
	    {
	      // Load level.
	      SceneManager.LoadScene(LvlNameDecode(_currentLevel));
	    }
	  }

	  /// <summary>
	  ///   Decode scene name.
	  /// </summary>
	  /// <param name="scene">Scene to decode</param>
	  /// <returns>Decoded scene name</returns>
	  /// <exception cref="Exception">Exception is thrown when given scene can't be loaded</exception>
	  private static string SceneNameDecode(Scenes scene)
	  {
	    switch(scene)
	    {
	      case Scenes.SPLASH:
	      {
	        return "00_Splash";
	      }
	      case Scenes.AUTH:
	      {
		      return "01_Auth";
	      }
	      case Scenes.MAIN_MENU:
	      {
		      return "02_MainMenu";
	      }
	      case Scenes.Game:
	      {
	        return "03_Game";
	      }
	      default:
	      {
	        throw new Exception("Cannot load scene ["+scene+"]");
	      }
	    }
	  }

	  /// <summary>
	  ///   Decode level name.
	  /// </summary>
	  /// <param name="lvl">Level to decode</param>
	  /// <returns>Decoded level name</returns>
	  /// <exception cref="Exception">Exception is thrown when given load can't be loaded</exception>
	  private static string LvlNameDecode(Lvls lvl)
	  {
	    switch(lvl)
	    {
	      default:
	      {
	        throw new Exception("Cannot load level ["+lvl+"]");
	      }
	    }
	  }
		
		#endregion
	}

	/// <summary>
	///   Class that manage levels and scenes.
	/// </summary>
	public class LevelManager : LevelManager<LevelManager>
	{
	}
}