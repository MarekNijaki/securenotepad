﻿using System;
using System.Collections;
using N_Core.N_Security;
using N_SN.N_SaveAndLoad;
using UnityEngine;

namespace N_SN.N_Auth
{
	/// <summary>
	///   Authentication controller.
	/// </summary>
	public class AuthController : MonoBehaviour
	{
		#region Events

		/// <summary>
		///   Event - fired after user logged in successful.
		/// </summary>
		public event Action LoggedInSuccessful;
		
		/// <summary>
		///   Event - fired after user not logged in successful.
		///   <para></para>
		///   string - error message.
		/// </summary>
		public event Action<string> LoggedInUnSuccessful;
		
		/// <summary>
		///   Event - fired after user registered successful.
		/// </summary>
		public event Action RegisteredSuccessful;
		
		/// <summary>
		///   Event - fired after user not registered successful.
		///   <para></para>
		///   string - error message.
		/// </summary>
		public event Action<string> RegisteredUnSuccessful;
		
		/// <summary>
		///   Event - fired after change of password was successful.
		/// </summary>
		public event Action PasswordChangeSuccessful;
		
		/// <summary>
		///   Event - fired after change of password was unsuccessful.
		///   <para></para>
		///   string - error message.
		/// </summary>
		public event Action<string> PasswordChangeUnSuccessful;
		
		#endregion
		
		#region Public fields

		/// <summary>
		///   Password.
		/// </summary>
		public static string Password { get; private set; }

		#endregion
		
		#region Protected and private fields
		
		/// <summary>
		///   String used for validation of hash.
		/// </summary>
		private const string HASH_VALIDATION_STRING = "Ala ma kota, kota ma Ala";

		/// <summary>
		///   Temporary new password.
		/// </summary>
		private string _tmpNewPassword = "";
		
		#endregion

		#region Public methods
		
		/// <summary>
		///   Log in.
		/// </summary>
		/// <param name="password">Password</param>
		public void LogIn(string password)
		{
			StartCoroutine(RequestLogIn(password));
		}

		/// <summary>
		///   User registration.
		///   <para></para>
		///   After registration user will be automatically logged in.
		/// </summary>
		/// <param name="password">Password</param>
		public void RegisterUser(string password)
		{
			StartCoroutine(RequestRegisterUser(password));
		}

		/// <summary>
		///   Change password.
		/// </summary>
		/// <param name="newPassword">New password</param>
		public void ChangePassword(string newPassword)
		{
			StartCoroutine(RequestChangePassword(newPassword));
		}

		#endregion
		
		#region Protected and private methods
		
		/// <summary>
		///   Register user request.
		/// </summary>
		/// <param name="password">Password</param>
		/// <returns>IEnumerator for coroutine</returns>
		private IEnumerator RequestRegisterUser(string password)
		{
			// MN:TO_DO:check if user of that name already exist
			if(string.IsNullOrEmpty(password))
			{
				Password = string.Empty;
				RegisteredUnSuccessful?.Invoke("Password cannot be null");
				yield break;
			}

			Password = password;
			
			string validationHash = HashAlgorithms.ComputeHMACHashFromPassword(HASH_VALIDATION_STRING, password);
			PlayerPrefs.SetString("validationHash", validationHash);
			
			SaveDataController.Instance.LoadedUnSuccessful += RegisterOnSaveDataLoadedUnSuccessful;
			SaveDataController.Instance.LoadedSuccessful += RegisterOnSaveDataLoadedSuccessful;
			SaveDataController.Instance.LoadSaveData(Password);
		}

		/// <summary>
		///   Log in request.
		/// </summary>
		/// <param name="password">Password</param>
		/// <returns>IEnumerator for coroutine</returns>
		private IEnumerator RequestLogIn(string password)
		{
			if(string.IsNullOrEmpty(password))
			{
				Password = string.Empty;
				LoggedInUnSuccessful?.Invoke("Password cannot be null");
				yield break;
			}
			
			Password = password;
			
			string validationHash = HashAlgorithms.ComputeHMACHashFromPassword(HASH_VALIDATION_STRING, password);
			if(validationHash != PlayerPrefs.GetString("validationHash"))
			{
				Password = string.Empty;
				LoggedInUnSuccessful?.Invoke("Wrong password");
				yield break;
			}
			
			SaveDataController.Instance.LoadedUnSuccessful += LoginOnSaveDataLoadedUnSuccessful;
			SaveDataController.Instance.LoadedSuccessful += LoginOnSaveDataLoadedSuccessful;
			SaveDataController.Instance.LoadSaveData(Password);
		}

		// MN:TO_DO: fix this
		private IEnumerator RequestChangePassword(string newPassword)
		{
			if(string.IsNullOrEmpty(newPassword))
			{
				Password = string.Empty;
				PasswordChangeUnSuccessful?.Invoke("New password cannot be null");
				yield break;
			}

			_tmpNewPassword = newPassword;
			
			SaveDataController.Instance.LoadedUnSuccessful += ChangePasswordOnSaveDataLoadedUnSuccessful;
			SaveDataController.Instance.LoadedSuccessful += ChangePasswordOnSaveDataLoadedSuccessful;
			SaveDataController.Instance.LoadSaveData(Password);
		}
		
		#endregion
		
		#region Events handlers

		private void RegisterOnSaveDataLoadedUnSuccessful(string errorMsg)
		{
			SaveDataController.Instance.LoadedUnSuccessful -= RegisterOnSaveDataLoadedUnSuccessful;
			
			RegisteredUnSuccessful?.Invoke(errorMsg);
		}

		private void RegisterOnSaveDataLoadedSuccessful(SaveData saveData)
		{
			SaveDataController.Instance.LoadedSuccessful -= RegisterOnSaveDataLoadedSuccessful;
			
			RegisteredSuccessful?.Invoke();
		}
		
		private void LoginOnSaveDataLoadedUnSuccessful(string errorMsg)
		{
			SaveDataController.Instance.LoadedUnSuccessful -= LoginOnSaveDataLoadedUnSuccessful;
			
			LoggedInUnSuccessful?.Invoke(errorMsg);
		}

		private void LoginOnSaveDataLoadedSuccessful(SaveData saveData)
		{
			SaveDataController.Instance.LoadedSuccessful -= LoginOnSaveDataLoadedSuccessful;
			
			LoggedInSuccessful?.Invoke();
		}
		
		private void ChangePasswordOnSaveDataLoadedUnSuccessful(string errorMsg)
		{
			SaveDataController.Instance.LoadedUnSuccessful -= ChangePasswordOnSaveDataLoadedUnSuccessful;
			_tmpNewPassword = string.Empty;
			PasswordChangeUnSuccessful?.Invoke(errorMsg);
		}

		private void ChangePasswordOnSaveDataLoadedSuccessful(SaveData saveData)
		{
			SaveDataController.Instance.LoadedSuccessful -= ChangePasswordOnSaveDataLoadedSuccessful;
			
			Password = _tmpNewPassword;
			string validationHash = HashAlgorithms.ComputeHMACHashFromPassword(HASH_VALIDATION_STRING, Password);
			PlayerPrefs.SetString("validationHash", validationHash);
			
			SaveDataController.Instance.SaveData(Password,saveData);
			_tmpNewPassword = string.Empty;
			PasswordChangeSuccessful?.Invoke();
		}
		
		#endregion
	}
}