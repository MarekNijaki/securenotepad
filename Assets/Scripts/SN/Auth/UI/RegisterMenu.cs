using N_Core.N_Levels;
using TMPro;
using UnityEngine;

namespace N_SN.N_Auth.N_UI
{
	/// <summary>
	///   Registration menu.
	/// </summary>
	public class RegisterMenu : MonoBehaviour
	{
		#region Serialized fields
		
		/// <summary>
		///   Password input text.
		/// </summary>
		[SerializeField]
		private TMP_InputField _passwordInputTxt;
		
		/// <summary>
		///   Password repeated input text.
		/// </summary>
		[SerializeField]
		private TMP_InputField _passwordRepeatedInputTxt;

		/// <summary>
		///   Text box holding status of operation.
		/// </summary>
		[SerializeField]
		private TMP_InputField _statusMessageTxt;
		
		#endregion
		
		#region Protected and private fields
		
		/// <summary>
		///   Authorization menu.
		/// </summary>
		private AuthMenu _authMenu;
		
		/// <summary>
		///   Authorization controller.
		/// </summary>
		private AuthController _authController;

		#endregion
		
		#region Public methods
		
		/// <summary>
		///   Register.
		/// </summary>
		public void Register()
		{
			if(_passwordInputTxt.text.Length < 9)
			{
				ShowStatusMsg("Password must have at least 9 characters");
				return;
			}
			
			if(_passwordInputTxt.text != _passwordRepeatedInputTxt.text)
			{
				ShowStatusMsg("Password and re entered password are not same");
				return;
			}

			_authController.RegisterUser(_passwordInputTxt.text);
		}
    
		/// <summary>
		///   Back to previous menu.
		/// </summary>
		public void BackToPreviousMenu()
		{
			gameObject.SetActive(false);
			_authMenu.gameObject.SetActive(true);
		}

		#endregion
		
		#region Protected and private methods

		/// <summary>
		///   Awake.
		/// </summary>
		private void Awake()
		{
			_authMenu = FindObjectOfType<AuthMenu>();
			_authController = FindObjectOfType<AuthController>();
		}

		/// <summary>
		///   On enable.
		/// </summary>
		private void OnEnable()
		{
			_authController.RegisteredSuccessful += AuthControllerOnRegisteredSuccessful;
			_authController.RegisteredUnSuccessful += AuthControllerOnRegisteredUnSuccessful;
		}

		/// <summary>
		///   On disable.
		/// </summary>
		private void OnDisable()
		{
			_authController.RegisteredSuccessful -= AuthControllerOnRegisteredSuccessful;
			_authController.RegisteredUnSuccessful -= AuthControllerOnRegisteredUnSuccessful;
		}

		/// <summary>
		///   Show status message.
		/// </summary>
		/// <param name="msg">Message to display</param>
		private void ShowStatusMsg(string msg)
		{
			_statusMessageTxt.text = msg;
			_statusMessageTxt.gameObject.SetActive(true);
		}

		#endregion
		
		#region Events handlers
		
		/// <summary>
		///   Event - fired after user registered successful.
		/// </summary>
		private void AuthControllerOnRegisteredSuccessful()
		{
			_statusMessageTxt.text = "Registration successful";
			_statusMessageTxt.GetComponentInChildren<TMP_Text>().color = Color.green;
			_statusMessageTxt.gameObject.SetActive(true);
			
			LevelManager.Instance.MainMenuLoad(0.5F);
		}

		/// <summary>
		///   Event - fired after user not registered successful.
		/// </summary>
		/// <param name="errorMsg">Error message</param>
		private void AuthControllerOnRegisteredUnSuccessful(string errorMsg)
		{
			ShowStatusMsg(errorMsg);
		}
		
		#endregion
	}
}