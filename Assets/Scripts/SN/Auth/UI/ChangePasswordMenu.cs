﻿using N_Core.N_Levels;
using N_SN.N_UI.N_MainMenu;
using TMPro;
using UnityEngine;

namespace N_SN.N_Auth.N_UI
{
	/// <summary>
	///   Change password menu.
	/// </summary>
	public class ChangePasswordMenu : MonoBehaviour
	{
		#region Serialized fields
		
		/// <summary>
		///   New password input text.
		/// </summary>
		[SerializeField]
		private TMP_InputField _newPasswordInputTxt;
		
		/// <summary>
		///   New password repeated input text.
		/// </summary>
		[SerializeField]
		private TMP_InputField _newPasswordRepeatedInputTxt;

		/// <summary>
		///   Text box holding status of operation.
		/// </summary>
		[SerializeField]
		private TMP_InputField _statusMessageTxt;
		
		#endregion
		
		#region Protected and private fields

		/// <summary>
		///   Main menu.
		/// </summary>
		private MainMenu _mainMenu;
		
		/// <summary>
		///   Authorization menu.
		/// </summary>
		private AuthMenu _authMenu;
		
		/// <summary>
		///   Authorization controller.
		/// </summary>
		private AuthController _authController;

		#endregion
		
		#region Public methods
		
		/// <summary>
		///   Change password.
		/// </summary>
		public void ChangePassword()
		{
			if(_newPasswordInputTxt.text.Length < 1)
			{
				ShowStatusMsg("Insert valid new password");
				return;
			}
			
			if(_newPasswordRepeatedInputTxt.text.Length < 1)
			{
				ShowStatusMsg("Insert valid new password repeated");
				return;
			}
			
			if(_newPasswordInputTxt.text.Length < 9)
			{
				ShowStatusMsg("New password must have at least 9 characters");
				return;
			}
			
			if(_newPasswordInputTxt.text != _newPasswordRepeatedInputTxt.text)
			{
				ShowStatusMsg("New password and re entered new password are not same");
				return;
			}

			_authController.ChangePassword(_newPasswordInputTxt.text);
		}
    
		/// <summary>
		///   Back to previous menu.
		/// </summary>
		public void BackToPreviousMenu()
		{
			gameObject.SetActive(false);

			if(_authMenu == null)
			{
				_mainMenu.gameObject.SetActive(true);
			}
			else
			{
				_authMenu.gameObject.SetActive(true);
			}
		}

		#endregion
		
		#region Protected and private methods

		/// <summary>
		///   Awake.
		/// </summary>
		private void Awake()
		{
			_mainMenu = FindObjectOfType<MainMenu>();
			_authMenu = FindObjectOfType<AuthMenu>();
			_authController = FindObjectOfType<AuthController>();
		}

		/// <summary>
		///   On enable.
		/// </summary>
		private void OnEnable()
		{
			_authController.PasswordChangeSuccessful += AuthControllerOnPasswordChangeSuccessful;
			_authController.PasswordChangeUnSuccessful += AuthControllerOnPasswordChangeUnSuccessful;
		}

		/// <summary>
		///   On disable.
		/// </summary>
		private void OnDisable()
		{
			_authController.PasswordChangeSuccessful -= AuthControllerOnPasswordChangeSuccessful;
			_authController.PasswordChangeUnSuccessful -= AuthControllerOnPasswordChangeUnSuccessful;
		}

		/// <summary>
		///   Show status message.
		/// </summary>
		/// <param name="msg">Message to display</param>
		private void ShowStatusMsg(string msg)
		{
			_statusMessageTxt.text = msg;
			_statusMessageTxt.gameObject.SetActive(true);
		}

		#endregion
		
		#region Events handlers
		
		/// <summary>
		///   Event - fired after change of password was successful.
		/// </summary>
		private void AuthControllerOnPasswordChangeSuccessful()
		{
			_statusMessageTxt.text = "Change of password was successful";
			_statusMessageTxt.GetComponentInChildren<TMP_Text>().color = Color.green;
			_statusMessageTxt.gameObject.SetActive(true);
			
			LevelManager.Instance.MainMenuLoad(0.5F);
		}

		/// <summary>
		///   Event - fired after change of password was unsuccessful.
		/// </summary>
		/// <param name="errorMsg">Error message</param>
		private void AuthControllerOnPasswordChangeUnSuccessful(string errorMsg)
		{
			ShowStatusMsg(errorMsg);
		}
		
		#endregion
	}
}