﻿using UnityEngine;
using UnityEngine.UI;

namespace N_SN.N_Auth.N_UI
{
	/// <summary>
	///   Authorization menu.
	/// </summary>
	public class AuthMenu : MonoBehaviour
	{
		#region Serialized fields

		/// <summary>
		///   Automatic log in for debugging purposes.
		/// </summary>
		[SerializeField]
		[Tooltip("Automatic log in for debugging purposes")]
		private bool _debugAutoLogIn;

		/// <summary>
		///   Register button.
		/// </summary>
		[SerializeField]
		[Tooltip("Register button")]
		private Button _registerBtn;
		
		/// <summary>
		///   Change password button.
		/// </summary>
		[SerializeField]
		[Tooltip("Change password button")]
		private Button _changePasswordBtn;
		
		#endregion
		
		#region Protected and private fields
		
		/// <summary>
		///   Register menu.
		/// </summary>
		private RegisterMenu _registerMenu;
		
		/// <summary>
		///   Change password menu.
		/// </summary>
		private ChangePasswordMenu _changePasswordMenu;
		
		/// <summary>
		///   Log in menu.
		/// </summary>
		private LogInMenu _logInMenu;

		#endregion

		#region Public methods
		
		/// <summary>
		///   Show register menu.
		/// </summary>
		public void ShowRegisterMenu()
		{
			gameObject.SetActive(false);
			_registerMenu.gameObject.SetActive(true);
		}
		
		/// <summary>
		///   Show change password menu.
		/// </summary>
		public void ShowChangePasswordMenu()
		{
			gameObject.SetActive(false);
			_changePasswordMenu.gameObject.SetActive(true);
		}
    
		/// <summary>
		///   Show log in menu.
		/// </summary>
		public void ShowLogInMenu()
		{
			gameObject.SetActive(false);
			_logInMenu.gameObject.SetActive(true);
		}

		#endregion
		
		#region Protected and private methods

		/// <summary>
		///   Awake.
		/// </summary>
		private void Awake()
		{
			_registerMenu = FindObjectOfType<RegisterMenu>();
			_changePasswordMenu = FindObjectOfType<ChangePasswordMenu>();
			_logInMenu = FindObjectOfType<LogInMenu>();
		}

		/// <summary>
		///   On start.
		/// </summary>
		private void Start()
		{
			_registerMenu.gameObject.SetActive(false);
			_changePasswordMenu.gameObject.SetActive(false);
			_logInMenu.gameObject.SetActive(false);
			
			// MN:TO_DO: fix this:
			if(true)
			{
				_registerBtn.gameObject.SetActive(true);
				_changePasswordBtn.gameObject.SetActive(false);
			}
			else
			{
				_registerBtn.gameObject.SetActive(false);
				_changePasswordBtn.gameObject.SetActive(true);
			}

			if(_debugAutoLogIn)
			{
				ShowLogInMenu();
			}
		}

		#endregion
	}
}