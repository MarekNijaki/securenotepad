using N_Core.N_Levels;
using TMPro;
using UnityEngine;

namespace N_SN.N_Auth.N_UI
{
	/// <summary>
	///   Log in menu.
	/// </summary>
	public class LogInMenu : MonoBehaviour
	{
		#region Serialized fields
		
		/// <summary>
		///   Automatic log in for debugging purposes.
		/// </summary>
		[SerializeField]
		[Tooltip("Automatic log in for debugging purposes")]
		private bool _debugAutoLogIn;
		
		/// <summary>
		///   Password input text.
		/// </summary>
		[SerializeField]
		private TMP_InputField _passwordInputTxt;

		/// <summary>
		///   Text box holding status of operation.
		/// </summary>
		[SerializeField]
		private TMP_InputField _statusMessageTxt;
		
		#endregion
		
		#region Protected and private fields
		
		/// <summary>
		///   Authorization menu.
		/// </summary>
		private AuthMenu _authMenu;
		
		/// <summary>
		///   Authorization controller.
		/// </summary>
		private AuthController _authController;

		#endregion
		
		#region Public methods
		
		/// <summary>
		///   Log in.
		/// </summary>
		public void LogIn()
		{
			// MN:TO_DO: uncomment
			// if(_passwordInputTxt.text.Length < 9)
			// {
			// 	ShowStatusMsg("Password must have at least 9 characters");
			// 	return;
			// }
			
			_authController.LogIn(_passwordInputTxt.text);
		}
    
		/// <summary>
		///   Back to previous menu.
		/// </summary>
		public void BackToPreviousMenu()
		{
			gameObject.SetActive(false);
			_authMenu.gameObject.SetActive(true);
		}

		#endregion
		
		#region Protected and private methods

		/// <summary>
		///   Awake.
		/// </summary>
		private void Awake()
		{
			_authMenu = FindObjectOfType<AuthMenu>();
			_authController = FindObjectOfType<AuthController>();
		}
		
		/// <summary>
		///   On enable.
		/// </summary>
		private void OnEnable()
		{
			_authController.LoggedInSuccessful += AuthControllerOnLoggedInSuccessful;
			_authController.LoggedInUnSuccessful += AuthControllerOnLoggedInUnSuccessful;

			if(_debugAutoLogIn)
			{
				_passwordInputTxt.text = "123456789";
				LogIn();
			}
		}

		/// <summary>
		///   On disable.
		/// </summary>
		private void OnDisable()
		{
			_authController.LoggedInSuccessful -= AuthControllerOnLoggedInSuccessful;
			_authController.LoggedInUnSuccessful -= AuthControllerOnLoggedInUnSuccessful;
		}

		/// <summary>
		///   Show status message.
		/// </summary>
		/// <param name="msg">Message to display</param>
		private void ShowStatusMsg(string msg)
		{
			_statusMessageTxt.text = msg;
			_statusMessageTxt.gameObject.SetActive(true);
		}
		
		#endregion
		
		#region Events handlers
		
		/// <summary>
		///   Event - fired after user logged in successful.
		/// </summary>
		private void AuthControllerOnLoggedInSuccessful()
		{
			_statusMessageTxt.text = "Log in successful";
			_statusMessageTxt.GetComponentInChildren<TMP_Text>().color = Color.green;
			_statusMessageTxt.gameObject.SetActive(true);
			
			LevelManager.Instance.MainMenuLoad(0.5F);
		}

		/// <summary>
		///   Event - fired after user not logged in successful.
		/// </summary>
		/// <param name="errorMsg">Error message</param>
		private void AuthControllerOnLoggedInUnSuccessful(string errorMsg)
		{
			ShowStatusMsg(errorMsg);
		}
		
		#endregion
	}
}