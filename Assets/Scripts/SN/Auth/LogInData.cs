﻿using System;
using UnityEngine;

namespace N_SN.N_Auth
{
	/// <summary>
	///   Structure used to to log in into the game.
	///   <para></para>
	///   Structure also used in registration (after successful registration, user will be automatically logged in).
	/// </summary>
	[Serializable]
	public struct LogInData
	{
		#region Public fields

		/// <summary>
		///   Error message (null if no error).
		/// </summary>
		public string ErrorMsg
		{
			get { return error; }
		}

		/// <summary>
		///   Flag indicating whether login was successful.
		/// </summary>
		public bool WasLoginSuccessful
		{
			get { return success; }
		}

		/// <summary>
		///   User ID.
		///   <para></para>
		///   Used across whole save and load data management.
		/// </summary>
		public int UserId
		{
			get { return userId; }
		}

		#endregion

		#region Protected and private fields

		/// <summary>
		///    MN:TO_DO: change name to 'errorMsg'
		///   Error message (null if no error).
		/// </summary>
		[SerializeField]
		private string error;

		/// <summary>
		///   Flag indicating whether login was successful.
		/// </summary>
		[SerializeField]
		private bool success;

		/// <summary>
		///   User ID.
		///   <para></para>
		///   Used across whole save and load data management.
		/// </summary>
		[SerializeField]
		private int userId;

		#endregion
	}
}