﻿using System;
using N_Core.N_Levels;
using N_SN.N_Auth;
using N_SN.N_SaveAndLoad;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
///   Game GUI.
/// </summary>
public class GameGUI : MonoBehaviour
{
	#region Serialized fields
	
	/// <summary>
	///   Clear button.
	/// </summary>
	[SerializeField]
	[Tooltip("Clear button")]
	private Button _clearBtn;
	
	/// <summary>
	///   Save button.
	/// </summary>
	[SerializeField]
	[Tooltip("Save button")]
	private Button _saveBtn;
	
	/// <summary>
	///   Main menu button.
	/// </summary>
	[SerializeField]
	[Tooltip("Main menu button")]
	private Button _mainMenuBtn;

	/// <summary>
	///   "Input field for content of notepad.
	/// </summary>
	[SerializeField]
	[Tooltip("Input field for content of notepad")]
	private TMP_InputField _contentInputField;

	#endregion
	
	#region Protected and private methods

	/// <summary>
	///   Start.
	/// </summary>
	private void Start()
	{
		SaveDataController.Instance.LoadedSuccessful += saveData => _contentInputField.text = saveData.NotepadContent;
		SaveDataController.Instance.LoadSaveData(AuthController.Password);
	}

	/// <summary>
	///   On enable.
	/// </summary>
	private void OnEnable()
	{
		_clearBtn.onClick.AddListener(Clear);
		_saveBtn.onClick.AddListener(Save);
		_mainMenuBtn.onClick.AddListener(MainMenu);
	}
	
	/// <summary>
	///   On disable.
	/// </summary>
	private void OnDisable()
	{
		_clearBtn.onClick.RemoveListener(Clear);
		_saveBtn.onClick.RemoveListener(Save);
		_mainMenuBtn.onClick.RemoveListener(MainMenu);
	}

	/// <summary>
	///   Clear text.
	/// </summary>
	private void Clear()
	{
		_contentInputField.text = string.Empty;
	}
	
	/// <summary>
	///   Save data.
	/// </summary>
	private void Save()
	{
		SaveData saveData = new SaveData(_contentInputField.text);
		SaveDataController.Instance.SaveData(AuthController.Password,saveData);
	}
	
	/// <summary>
	///   Load main menu.
	/// </summary>
	private static void MainMenu()
	{
		LevelManager.Instance.MainMenuLoad(0.0F);
	}
	
	#endregion
}