﻿using N_Core.N_Levels;
using N_SN.N_Auth.N_UI;
using UnityEngine;

namespace N_SN.N_UI.N_MainMenu
{
  /// <summary>
  ///   Main menu.
  /// </summary>
  public class MainMenu : MonoBehaviour
  {
    #region Serialized fields

    /// <summary>
    ///   Change password menu.
    /// </summary>
    [SerializeField]
    [Tooltip("Change password menu")]
    private ChangePasswordMenu _changePasswordMenu; 
    
    #endregion
    
    #region Public methods

    /// <summary>
    ///   Quit.
    /// </summary>
    public void Quit()
    {
      LevelManager.Instance.Quit();
    }

    /// <summary>
    ///   StartProgram.
    /// </summary>
    public void StartProgram()
    {
      // Load next level.
      LevelManager.Instance.GameLoad(0.0F);
      // Disable main menu.
      gameObject.SetActive(false);
    }
    
    /// <summary>
    ///   Change password.
    /// </summary>
    public void ChangePassword()
    {
      _changePasswordMenu.gameObject.SetActive(true);
      
      // Disable main menu.
      gameObject.SetActive(false);
    }

    #endregion
  }
}