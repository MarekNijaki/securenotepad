﻿using System;
using UnityEngine;

namespace N_SN.N_SaveAndLoad
{
	/// <summary>
	///   Save data.
	/// </summary>
	[Serializable]
	public class SaveData
	{
		#region Public fields

		/// <summary>
		///   Content of notepad text to save.
		/// </summary>
		public string NotepadContent
		{
			get { return _notepadContent; }
		}
		
		/// <summary>
		///   Hash of content (used for data integrity).
		/// </summary>
		public string HashOfContent { get; set; }

		#endregion

		#region Protected and private fields

		/// <summary>
		///   Content of notepad text to save.
		/// </summary>
		[SerializeField]
		private string _notepadContent;

		#endregion
		
		#region Public methods

		/// <summary>
		///   Constructor.
		/// </summary>
		/// <param name="notepadContent">Text to save</param>
		public SaveData(string notepadContent)
		{
			_notepadContent = notepadContent;
		}
		
		#endregion
	}
}