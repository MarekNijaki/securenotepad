﻿using System;
using System.Collections;
using System.IO;
using N_Core.N_Security;
using N_Core.N_Singletons;
using UnityEngine;

namespace N_SN.N_SaveAndLoad
{
	/// <summary>
	///   Save data controller.
	/// </summary>
	public abstract class SaveDataController<TInstance> : UnitySingleton<TInstance> where TInstance : Component
	{
		#region Events
		
		/// <summary>
		///   Event - fired after save data was loaded successful.
		///   <para></para>
		///   Retrieved save data.
		/// </summary>
		public event Action<SaveData> LoadedSuccessful;
		
		/// <summary>
		///   Event - fired after save data was not loaded successful.
		///   <para></para>
		///   string - error message.
		/// </summary>
		public event Action<string> LoadedUnSuccessful;
		
		/// <summary>
		///   Event - fired after save data was saved successful.
		/// </summary>
		public event Action SavedSuccessful;
		
		/// <summary>
		///   Event - fired after save data was not saved successful.
		///   <para></para>
		///   string - error message.
		/// </summary>
		public event Action<string> SavedUnSuccessful;
		
		#endregion
		
		#region Protected and private fields

		/// <summary>
		///   URL for loading SaveData.
		/// </summary>
		private const string SAVE_DATA_LOAD_URL = "http://185.243.52.104/action_load.php";
		
		/// <summary>
		///   URL for saving SaveData.
		/// </summary>
		private const string SAVE_DATA_SAVE_URL = "http://185.243.52.104/action_save.php";
		
		/// <summary>
		///   Path to save file.
		/// </summary>
		private string PATH_TO_SAVE_FILE;

		#endregion
		
		#region Public methods
		
		/// <summary>
		///   Load save data.
		/// </summary>
		/// <param name="password">Password to use to load save data</param>
		public void LoadSaveData(string password)
		{
			StartCoroutine(RequestLoadSaveData(password));
		}

		/// <summary>
		///   Save data.
		/// </summary>
		/// <param name="password">Password to use to save data</param>
		/// <param name="saveData">Data to save</param>
		public void SaveData(string password, SaveData saveData)
		{
			StartCoroutine(RequestSaveData(password, saveData));
		}

		#endregion
		
		#region Protected and private methods

		/// <summary>
		///    Initialization.
		/// </summary>
		protected override void Init()
		{
			base.Init();
			PATH_TO_SAVE_FILE = Path.Combine(Application.persistentDataPath, "SecureNotepadData/saveData.dat");
		}

		/// <summary>
		///   Request for load of save data.
		/// </summary>
		/// <param name="password">Password to use to load save data</param>
		/// <returns>IEnumerator for coroutine</returns>
		private IEnumerator RequestLoadSaveData(string password)
		{
			// If password is null then exit.
			if(string.IsNullOrEmpty(password))
			{
				LoadedUnSuccessful?.Invoke("Password cannot be null");
				yield break;
			}

			// Save data.
			SaveData saveData;
			// If file exist then load save data.
			if(File.Exists(PATH_TO_SAVE_FILE))
			{
				try
				{
					// Read encrypted data.
					byte[] encryptedData = File.ReadAllBytes(PATH_TO_SAVE_FILE);
					// Decrypt data.
					string json = TripleDESAlgorithm.Decrypt(password, encryptedData);
					// Convert JSON to save data.
					saveData = JsonUtility.FromJson<SaveData>(json);
				}
				catch(Exception)
				{
					LoadedUnSuccessful?.Invoke("Unhandled error during loading of save file");
					yield break;
				}
			}
			// If no file then create default save data.
			else
			{
				saveData = new SaveData("");
			}
			
			// Send message.
			LoadedSuccessful?.Invoke(saveData);
		}

		/// <summary>
		///   Request for save data.
		/// </summary>
		/// <param name="password">Password to use to save data</param>
		/// <param name="saveData">Data to save</param>
		/// <returns>IEnumerator for coroutine</returns>
		private IEnumerator RequestSaveData(string password, SaveData saveData)
		{
			// If save data is null then exit.
			if(saveData == null)
			{
				SavedUnSuccessful?.Invoke("Save data cannot be null");
				yield break;
			}
			
			// If password is null then exit.
			if(string.IsNullOrEmpty(password))
			{
				SavedUnSuccessful?.Invoke("Password cannot be null");
				yield break;
			}

			// If director not exist then create directory from path to save.
			if(!Directory.Exists(Path.GetDirectoryName(PATH_TO_SAVE_FILE)))
			{
				Directory.CreateDirectory(Path.GetDirectoryName(PATH_TO_SAVE_FILE));
			}
			// Convert save data content to string.
			string json = JsonUtility.ToJson(saveData, true);
			// Encrypt data.
			byte[] encryptedData = TripleDESAlgorithm.Encrypt(password, json);
			// Save data.
			File.WriteAllBytes(PATH_TO_SAVE_FILE,encryptedData);
			// Send message.
			SavedSuccessful?.Invoke();
		}

		#endregion
	}
	
	/// <summary>
	///   Save data controller.
	/// </summary>
	public class SaveDataController : SaveDataController<SaveDataController>
	{
	}
}